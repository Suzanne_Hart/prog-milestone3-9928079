﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9928079

{
    public class Client
    {
        public string Name;
        public string Phone;

        public Client(string _name, string _phone)
        {
            Phone = _phone;
            Name = _name;
        }

        public class Pizza
        {
            public string Type1;
            public string Type2;
            public string Type3;
            public string Type4;

            public static List<Tuple<string, string, double>> Totalorder = new List<Tuple<string, string, double>>();

            public Pizza()
            {
                Type1 = "Hawaiian";
                Type2 = "Cheesy Weesy";
                Type3 = "Supreme";
                Type4 = "Vegetarian";
            }
        }

        public class Mainclass
        {
            public static void Main(string[] args)

            {
                try
                {
                    PageSpace();
                    PageSpace();
                    Console.WriteLine("             ****************************************************************************************");
                    Console.WriteLine("             *                                                                                      *");
                    Console.WriteLine("             *                                                                                      *");
                    Console.WriteLine("             *                             Welcome to Pizza World!!                                 *");
                    Console.WriteLine("             *    To assist me with giving you great service, please enter a name for your order    *");
                    Console.WriteLine("             *                                                                                      *");
                    Console.WriteLine("             *                                                                                      *");
                    Console.WriteLine("             ****************************************************************************************");
                    var a = Console.ReadLine();

                    Console.WriteLine("Great, thanks! Please tell me your phone number");
                    string phone = Console.ReadLine();

                    Client client = new Client(a, phone);
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine($"You're doing great {client.Name}! Please take a look at the menu and let me know what you would like to order");
                    Menu();
                }
                catch
                {
                    Console.WriteLine("Unfortunately, there has been an error. Please try again...");
                }
            }

            public static void Menu()
            {
                var choice = 0;
                PageSpace();
                Console.WriteLine("                                                  *  PIZZA MENU  *                                   ");
                Console.WriteLine("             ****************************************************************************************");
                Console.WriteLine("             *                                                                                      *");
                Console.WriteLine("             *                                     1. Hawaiian                                      *");
                Console.WriteLine("             *                                     2. Cheesy Weesy                                  *");
                Console.WriteLine("             *                                     3. Supreme                                       *");
                Console.WriteLine("             *                                     4. Vegetarian                                    *");
                Console.WriteLine("             *                                                                                      *");
                Console.WriteLine("             ****************************************************************************************");
                Console.WriteLine("                                              Please select an option...                             ");
                choice = int.Parse(Console.ReadLine());

                if (choice == 1)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose 'Hawaiian'");
                    PizzaChoice("Hawaiian");

                }
                else if (choice == 2)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose 'Cheesy Weesy'");
                    PizzaChoice("Cheesy Weesy");
                }
                else if (choice == 3)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose 'Supreme'");
                    PizzaChoice("Supreme");
                }
                else if (choice == 4)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose 'Vegetarian'");
                    PizzaChoice("Vegetarian");

                }
                else
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("I'm sorry but that is not a valid option");
                    Console.WriteLine("Please try again");
                    Menu();
                }
            }

            public static void PizzaChoice(string selection)
            {
                var choice = 0;
                var pizzasize = new List<Tuple<string, string, double>>();
                PageSpace();

                Console.WriteLine("What size pizza would you like?");
                PageSpace();
                Console.WriteLine("                                                  *  PIZZA SIZE  *                                   ");
                Console.WriteLine("             ****************************************************************************************");
                Console.WriteLine("             *                                                                                      *");
                Console.WriteLine("             *                                     1. Small                                         *");
                Console.WriteLine("             *                                     2. Medium                                        *");
                Console.WriteLine("             *                                     3. Large                                         *");
                Console.WriteLine("             *                                                                                      *");
                Console.WriteLine("             ****************************************************************************************");
                Console.WriteLine("                                            Please select an option...                               ");

                choice = int.Parse(Console.ReadLine());

                if (choice == 1)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose 'Small'");
                    Pizza.Totalorder.Add(Tuple.Create(selection, "small", 5.00));
                    MorePizza();
                }
                else if (choice == 2)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose 'Medium'");
                    Pizza.Totalorder.Add(Tuple.Create(selection, "medium", 8.00));
                    MorePizza();
                }
                else if (choice == 3)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose 'Large'");
                    Pizza.Totalorder.Add(Tuple.Create(selection, "large", 10.00));
                    MorePizza();
                }
                else
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("I'm sorry but that is not a valid option");
                    Console.WriteLine("Please try again");
                    Menu();
                }
            }

            public static void MorePizza()
            {
                var choice = 0;
                PageSpace();
                Console.WriteLine("                                      *  Would you like to order another pizza?  *                                   ");
                Console.WriteLine("             ****************************************************************************************");
                Console.WriteLine("             *                                                                                      *");
                Console.WriteLine("             *                                     1. Yes                                           *");
                Console.WriteLine("             *                                     2. No                                            *");
                Console.WriteLine("             *                                                                                      *");
                Console.WriteLine("             ****************************************************************************************");
                Console.WriteLine("                                            Please select an option...                               ");

                choice = int.Parse(Console.ReadLine());

                if (choice == 1)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose Yes");
                    Menu();
                }
                else if (choice == 2)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose no");
                    Drinks();
                }
                else
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("I'm sorry but that is not a valid option");
                    Console.WriteLine("Please try again");
                    MorePizza();
                }
            }

            public static void Drinks()
            {
                var choice = 0;
                Console.Clear();
                PageSpace();

                Console.WriteLine("What drink would you like?");
                PageSpace();
                Console.WriteLine("                                                 *  DRINKS LIST  *                                   ");
                Console.WriteLine("             ****************************************************************************************");
                Console.WriteLine("             *                                                                                      *");
                Console.WriteLine("             *                                     1. Coke                                          *");
                Console.WriteLine("             *                                     2. Fanta                                         *");
                Console.WriteLine("             *                                     3. Orange Juice                                  *");
                Console.WriteLine("             *                                     4. Milk                                          *");
                Console.WriteLine("             *                                                                                      *");
                Console.WriteLine("             ****************************************************************************************");
                Console.WriteLine("                                             Please select an option...                              ");
                choice = int.Parse(Console.ReadLine());

                if (choice == 1)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose 'Coke'");
                    Pizza.Totalorder.Add(Tuple.Create("Coke", "Medium", 2.00));
                    MoreDrink();
                }
                else if (choice == 2)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose 'Fanta'");
                    Pizza.Totalorder.Add(Tuple.Create("Fanta", "Medium", 2.00));
                    MoreDrink();
                }
                else if (choice == 3)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose 'Orange Juice'");
                    Pizza.Totalorder.Add(Tuple.Create("Orange Juice", "Medium", 2.00));
                    MoreDrink();
                }
                else if (choice == 4)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose 'Milk'");
                    Pizza.Totalorder.Add(Tuple.Create("Milk", "Medium", 2.00));
                    MoreDrink();
                }
                else
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("I'm sorry but that is not a valid option");
                    Console.WriteLine("Please try again");
                    Drinks();
                }
            }

            public static void MoreDrink()
            {
                var choice = 0;
                PageSpace();
                Console.WriteLine("                                      *  Would you like to order another drink?  *                                   ");
                Console.WriteLine("             ****************************************************************************************");
                Console.WriteLine("             *                                                                                      *");
                Console.WriteLine("             *                                     1. Yes                                           *");
                Console.WriteLine("             *                                     2. No                                            *");
                Console.WriteLine("             *                                                                                      *");
                Console.WriteLine("             ****************************************************************************************");
                Console.WriteLine("                                            Please select an option...                               ");

                choice = int.Parse(Console.ReadLine());

                if (choice == 1)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose Yes");
                    Drinks();
                }
                else if (choice == 2)
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("You chose no");
                    Payment();
                }
                else
                {
                    Console.Clear();
                    PageSpace();
                    Console.WriteLine("I'm sorry but that is not a valid option");
                    Console.WriteLine("Please try again");
                    MoreDrink();
                }
            }

            public static void Payment()
            {
                Console.Clear();
                var totalprice = 0.00;
                double cashpaid = 0.00;
                PageSpace();
                Console.WriteLine("Thank You! These are the items that you have ordered");

                foreach (Tuple<string, string, double> x in Pizza.Totalorder)
                {
                    Console.WriteLine($"{x.Item1} = ${x.Item3:F2}");
                }

                Console.WriteLine();
                foreach (Tuple<string, string, double> x in Pizza.Totalorder)
                {
                    totalprice += x.Item3;
                }

                Console.WriteLine($"The total price for your order is ${totalprice:F2}");

                Console.WriteLine("Please enter your cash amount paid using the following format e.g. 10.00");
                cashpaid = double.Parse(Console.ReadLine());
                Console.WriteLine($"Thank You.  The amount you entered is ${cashpaid:F2}");
                var change = cashpaid - totalprice;

                if (change >= 0)
                {
                    Console.WriteLine($"Your change is ${change:F2}");

                    Console.WriteLine("Thank You for shopping at Pizza World!! Please come again!");
                    Console.WriteLine("");
                    Console.WriteLine("Pressing the enter key will end this program...");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("");
                    Console.WriteLine("Unfortunately, that is not enough money for this transaction");
                    Console.WriteLine("Please press enter to try again...");
                    Console.ReadLine();
                    Payment();
                }
            }

    public static void PageSpace()
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
            }
        }
    }
}
